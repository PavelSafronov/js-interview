const { sum } = require('./app');
const assert = require("assert");

describe("sum function", () => {
    it("sum of 2 and 3 should be 5", () => {
        assert.equal(sum(2, 2), 4);
        sum(2, 3);
    });

    it("sum of 2 and 3 should not be 3", () => {
        assert.notEqual(sum(2, 3), 3);
        sum(2, 3);
    });
});